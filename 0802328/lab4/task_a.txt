prefix ex:<http://GP-onto.fi.upm.es/exercise2#>


1)
select distinct ?type
where {
  ?s a ?type.
}

2)
select distinct ?subclass
where {
 ?subclass rdfs:subClassOf ex:Establishment.
}

3)
select distinct ?instance 
where{
 ?instance a ex:City.
}

4)
select ?inhabitants
where{
 ex:Santiago_de_Compostela ex:hasInhabitantNumber ?inhabitants.
}

5)
select ?place ?inhabitants 
where{
 ?place ex:hasInhabitantNumber ?inhabitants.
 FILTER(?place=ex:Arzua or ?place=ex:Santiago_de_Compostela)
}

6)
select ?place ?inhabitants 
where{
 ?place ex:hasInhabitantNumber ?inhabitants.
} order by (?place)

7)
select ?instance ?inhabitants 
where{
 ?class rdfs:subClassOf ex:Locality.
 ?instance a ?class.
 optional { ?instance ex:hasInhabitantNumber ?inhabitants. }
}

8)
select ?place ?inhabitants 
where{
 ?place ex:hasInhabitantNumber ?inhabitants.
 filter(xsd:integer(?inhabitants) > 200000)
}

9)
select ?street ?number ?city ?province
where{
 ex:Pazo_Breogan ex:hasAddress ?address.
 ex:Pazo_Breogan ex:isPlacedIn ?c.

 ?c rdfs:label ?city.
 ?c ex:inProvince ?province.
 ?address ex:hasNumber ?number.
 ?address ex:hasStreet ?street.
}

10)
select distinct ?subclass
where {
 ?subclass rdfs:subClassOf ex:Location.
}

11)
select distinct ?instance 
where {
 ?instance a ?class.
 ?class rdfs:subClassOf ex:Locality.
}

12)
describe ?resource
where {
 ?resource rdfs:label "Madrid".
}

13)
construct {
 ?place ex:isIn ?province
}
where {
 select ?place ?province
 where {
  ?class rdfs:subClassOf ex:TouristicLocation.
  ?place a ?class.
  ?place ex:isPlacedIn ?city.
  ?city ex:inProvince ?province.
 }
}

14)
ask{ 
 ?s ?p ex:Town 
}





