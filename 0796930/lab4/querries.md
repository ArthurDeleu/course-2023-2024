# Lab 4 querries
1. 
```
SELECT ?class WHERE { ?class a rdfs:Class .}
```
2. 
```
SELECT ?a
WHERE {
  ?a rdfs:subClassOf* <http://GP-onto.fi.upm.es/exercise2#Establishment>.
}
```
3. 
```
SELECT ?a
WHERE {
  ?a a <http://GP-onto.fi.upm.es/exercise2#City>.
}
```
4. 
```
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

SELECT ?a
WHERE {
  ex:Santiago_de_Compostela ex:hasInhabitantNumber ?a.
}
```
5.
```
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

SELECT ?a1 ?a2
WHERE {
  ex:Santiago_de_Compostela ex:hasInhabitantNumber ?a1.
  ex:Arzua ex:hasInhabitantNumber ?a2.
}
```

6. 
```
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

SELECT ?s ?o
WHERE {
  ?s ex:hasInhabitantNumber ?o.
} ORDER BY ?o
```

7. 
```
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

SELECT ?o
WHERE {
  ?types rdfs:subClassOf ex:Locality .
  ?s a ?types .
  FILTER EXISTS {?s ex:hasInhabitantNumber ?o .}
  ?s ex:hasInhabitantNumber ?o.
}
```

8. 
```
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

SELECT ?s ?o
WHERE {
  ?s ex:hasInhabitantNumber ?o.
  FILTER (xsd:integer(?o) > 200000)
}
```

9. 
```

```