1. Get all the classes

PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

select distinct ?class
where{
 ?class a rdfs:Class . 
}

2. Get all the subclasses of the class Establishment

PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

select ?s
where{
 ?s rdfs:subClassOf ex:Establishment
}


3. Get all instances of class City

prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

select  ?s
where{
 ?s rdf:type ex:City

}


4. Get the number of inhabitants of Santiago de Compostela


Santiago_de_Compostela

hasInhabitantNumber 	



prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

select  ?o
where{
 ex:Santiago_de_Compostela ex:hasInhabitantNumber  ?o
}



5. Get the number of inhabitants of Santiago de Compostela and Arzua


prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

select  ?o
where{
 values ?place {ex:Santiago_de_Compostela ex:Arzua}
 ?place ex:hasInhabitantNumber  ?o

}

6. Get different places with their number of inhabitants, ordered by the name of the place


prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

select  ?place ?o
where{
 ?place ex:hasInhabitantNumber  ?o

}
order by ?place

7. Get all instances of the subclass Locality with their number of inhabitants (if it exists)

prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

select ?s ?place ?o
where{
 ?s rdfs:subClassOf ex:Locality.
 ?place rdf:type  ?s.
 optional{?place ex:hasInhabitantNumber  ?o.}
}


8. Get all places with more than 200.000 inhabitants

prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

select  ?place ?o
where{
 ?place ex:hasInhabitantNumber  ?o
 filter( xsd:integer(?o) > 200000)

}

9. Get all postal address details of Pazo_Breogan

prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

select ?o
where{
 ex:Pazo_Breogan ex:hasAddress ?o 
}

10. Get all subclasses of class Location

prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

select ?s
where{
 ?s rdfs:subClassOf ex:Location
}

11. Get all instances of class Locality

prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

select ?s ?place 
where{
 ?s rdfs:subClassOf ex:Locality.
 ?place rdf:type  ?s.
 
}

12. Describe the resource with rdfs:label "Madrid”

prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

describe  ?s
where{
 ?s rdfs:label "Madrid"

}


13. Construct the RDF(S) graph that relates all touristic places with their provinces, using a new property ”isIn”

prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

construct{?place ex:isIn ?province}
where
{
 select ?place ?province where
 {
  ?class rdfs:subClassOf ex:TouristicLocation.
  ?place a ?class.
  ?place ex:isPlacedIn ?city.
  ?city ex:inProvince ?province.
 }
}

}


14. Ask whether there is any instance of Town

prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

ask{
 ?s rdf:type ex:Town

}




