PREFIX ex: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX ontology: <http://data.sfgov.org/ontology#>

1)
select distinct ?class where{
    ?s a ?class .
}

2)
select distinct ?property where{
    ?instance a ?class .
    ?instance ?property ?value .
}

3)
select distinct ?property where{
    ?instance a ontology:Aircraft .
    ?instance ?property ?value .
    filter(?property != rdf:type)
}

4)
ANSWER: 14993

select (count(distinct ?value) as ?count) where{
    ?instance a ontology:Aircraft .
    ?instance ?property ?value .
    filter(?property != rdf:type)
}

5)
select ?property (count(distinct ?value) as ?count) where{
    ?instance a ontology:Aircraft .
    ?instance ?property ?value .
    filter(?property != rdf:type)
}
group by ?property

6)
ANSWER: 2366.5

select (avg(?count) as ?average) where{
    select (count(distinct ?value) as ?count) where{
        ?instance a ontology:Aircraft .
        ?instance ?property ?value .
        filter(?property != rdf:type)
	}
    group by ?property
}

7)
ANSWER:
hasAircraftId: 5221
hasTailNumber: 4510
...

select ?property (count(distinct ?value) as ?count) where{
    ?instance a ontology:Aircraft .
    ?instance ?property ?value .
    filter(?property != rdf:type)
}
group by ?property
order by desc(?count)
