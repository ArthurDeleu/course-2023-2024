@prefix dash: <http://datashapes.org/dash#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema: <http://schema.org/> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLschema#> .

schema:ActivityPeriodShape
	a sh:NodeShape ;
	sh:targetClass schema:ActivityPeriod ;
	sh:property [
		sh:path schema:has_activity_period_value ;
		sh:datatype xsd:string ;
	]

schema: AircraftShape
	a sh:NodeShape ;
	sh:targetClass schema:Aircraft ;
	sh:property [ 
		sh:path schema:has_aircraft_id ;
		sh:datatype xsd:int ;
	sh:property [
        	sh:path schema:has_creation_date ;
        	sh:datatype xsd:dateTime ;
	] ;
    	sh:property [
        	sh:path schema:has_modification_date ;
		sh:lessThan schema:has_creation_date ;
        	sh:datatype xsd:date ;
    	] ;
    	sh:property [
        	sh:path schema:has_tail_number ;
        	sh:datatype xsd:string ;
    	] ;
    	sh:property [
        	sh:path schema:is_active ;
        	sh:datatype xsd:boolean ;
    	] ;
    	sh:property [
        	sh:path schema:owning_airline ;
        	sh:datatype rdfs:Literal ;
    	] ;
    	sh:property [
        	sh:path schema:has_aircraft_model ;
        	sh:class schema:AircraftModel ;
    	] .


schema:AircraftModelShape 
	a sh:NodeShape ;
    	sh:targetClass schema:AircraftModel ;
    	sh:property [
        	sh:path schema:has_model_name ;
        	sh:datatype rdfs:Literal ;
    	] ;
	sh:property [
		sh:path schema has_aircraft_body_type ;
		sh:in ("narrow_body", "regional_jet", "turbo_prop", "wide_body")
	] .


schema:GeoRegionShape a sh:NodeShape ;
    	sh:targetClass schema:GeoRegion ;
    	sh:property [
        	sh:path schema:has_region_summary ;
        	sh:datatype rdfs:Literal ;
    	] ;
    	sh:property [
        	sh:path schema:has_region_name ;
        	sh:datatype rdfs:Literal ;
    	] .
	