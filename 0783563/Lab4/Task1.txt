####### Task 1 queries #######

----- Prefixes -----
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

----- Queries -----
Query1:
select ?s where { ?s a rdfs:Class}

Query2:
select distinct ?s where { ?s rdfs:subClassOf ex:Establishment}

Query3:
select ?s where { ?s a ex:City}

Query4:
select ?o where { ex:Santiago_de_Compostela ex:hasInhabitantNumber ?o}

Query5:
select ?l ?o where { 
    {ex:Santiago_de_Compostela ex:hasInhabitantNumber ?o.
     ex:Santiago_de_Compostela rdfs:label ?l}
    UNION
    {ex:Arzua ex:hasInhabitantNumber ?o.
     ex:Arzua rdfs:label ?l}
}

Query6:
select ?s ?o where { 
    ?s ex:hasInhabitantNumber ?o.

} order by (?s)

Query7:
select ?s ?o where { 
    ?sub rdfs:subClassOf ex:Locality.
    ?s a ?sub.
    OPTIONAL{
        ?s ex:hasInhabitantNumber ?o.
    }
}

Query8:
select ?s ?o where { 
    ?s ex:hasInhabitantNumber ?o.
    FILTER(xsd:integer(?o) > 200000).
}

Query9:
describe ?o where { 
    ex:Pazo_Breogan ex:hasAddress ?o.
} 

Query10:
select distinct ?s where { ?s rdfs:subClassOf ex:Location}

Query11:
select ?s where {
   ?sub rdfs:subClassOf ex:Locality.
   ?s a ?sub.
}

Query12:
describe ?s where { 
    ?s rdfs:label "Madrid".
} 

Query13:
construct {?s ex:isIn ?o}
where {
    ?sub rdfs:subClassOf ex:TouristicLocation.
    ?s a ?sub.
    ?s ex:isPlacedIn ?x.
    ?x ex:inProvince ?o.
}

Query14:
ask { ?x a ex:Town}


